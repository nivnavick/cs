import java.util.Scanner;

public class Instrument {
    private final String INVALID_PRICE_ERR="Price must be a positive number!";
    private double price;
    private String manufacturer;

    public Instrument(double price,String manufacturer) throws AfekaInstrumentsException {
        setPrice(price);
        setManufacturer(manufacturer);
    }

    public Instrument(Scanner scanner) throws AfekaInstrumentsException {
        setPrice(Double.parseDouble(scanner.nextLine()));
        setManufacturer(scanner.nextLine());
    }

    public void setPrice(double price) throws AfekaInstrumentsException {
        if(price<=0) {
            throw new AfekaInstrumentsException(INVALID_PRICE_ERR);
        }
        this.price=price;
    }

    public void setManufacturer(String  manufacturer) {
        this.manufacturer=manufacturer;
    }

    public double getPrice() {
        return this.price;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    @Override
    public boolean equals(Object obj) {
        Instrument instrument=(Instrument)obj;
        return this.getPrice()==instrument.getPrice() && this.getManufacturer().equals(instrument.getManufacturer());
    }

    @Override
    public String toString() {
        return String.format("Price:%.2f Manufacturer:%s",price,manufacturer);
    }
}