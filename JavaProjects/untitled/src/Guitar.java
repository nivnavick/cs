import java.util.Scanner;

public class Guitar extends StringInstrument {

    public Guitar(double price, String manufacturer, int stringCount,String type) throws AfekaInstrumentsException {
        super(price, manufacturer, stringCount,type);
        setType(type);
    }

    public Guitar(Scanner scanner) throws AfekaInstrumentsException {
        super(scanner);
    }

    public void setType(String type) {
        super.setType(type);
    }

    public InstrumentType getType() {
        return super.getType();
    }

    @Override
    public String toString() {

        return String.format("%-10s %-10s |%s| Type: %s",
                super.getManufacturer(),
                this.getClass().getSimpleName(),
                super.toString(),
                getType().name());
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Guitar && super.equals(obj);
    }
}
