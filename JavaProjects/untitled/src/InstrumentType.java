public enum InstrumentType {
    Acoustic,
    Electric,
    Recorder,
    Classic,
    Bass,
    SideFluet
}