public class AfekaInstrumentsException extends Exception {
    public AfekaInstrumentsException(String message) {
        super(message);
    }
}
