import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class AfekaInstruments {

    public static void main(String[] args) throws FileNotFoundException, CloneNotSupportedException, AfekaInstrumentsException {

        try {
            ArrayList allInstruments= loadAllInstruments(loadFile().getAbsolutePath());
            if(allInstruments.size()==0) {
                return;
            }

            printInstruments(allInstruments);

            System.out.println("\nDifferent Instruments: "+getNumOfDifferentElements(allInstruments));
            System.out.println("\nMost Expensive Instrument:\n"+getMostExpensiveInstrument(allInstruments).toString());
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private static void printInstruments(ArrayList<Instrument> instruments) {
        for (Instrument in : instruments) {
            System.out.println(in.toString());
        }
    }

    private static File loadFile() {
        Scanner scanner =new Scanner(System.in);
        File file;
        Boolean fileExist=false;
        System.out.println("Please enter instruments file name / path:");
        do {
            file=new File(scanner.nextLine());
            fileExist=file.exists();
            if(!fileExist) {
                System.out.println("File Error! Please try again:");
            }
        }
        while (!fileExist);
        scanner.close();
        return file;
    }

    private static void addAllInstruments(ArrayList<Instrument> instruments,ArrayList<Instrument> toAdd)  {
        for(Instrument ins:toAdd) {
            instruments.add(ins);
        }
    }

    private static Instrument getMostExpensiveInstrument(ArrayList<Instrument> ins) {
        Instrument mostExpensive=null;
        for (int i = ins.size() - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (ins.get(j).getPrice() > ins.get(j + 1).getPrice())
                    mostExpensive=ins.get(j);
            }
        }
        return mostExpensive;
    }

    private static int getNumOfDifferentElements(ArrayList allInstruments) {
        int numOfDiffs=0;
        int size=allInstruments.size();
        int[] diffs=new int[size];
        for (int i = 0; i < size; i++) {
            for(int j=i+1;j<size;j++) {
                if(diffs[i]==0 && allInstruments.get(i).equals(allInstruments.get(j))) {
                    numOfDiffs++;
                    diffs[i]=1;
                }
            }
        }
        return size-numOfDiffs;
    }

    public static ArrayList loadAllInstruments(String filePath) throws FileNotFoundException, AfekaInstrumentsException {
        Scanner scanner = new Scanner(new File(filePath));
        ArrayList allInstruments = new ArrayList();
        addAllInstruments(allInstruments, parseGuitars(scanner));
        addAllInstruments(allInstruments, parseBassGuitars(scanner));
        addAllInstruments(allInstruments, parseFluet(scanner));
        addAllInstruments(allInstruments, parseSaxophone(scanner));
        scanner.close();
        System.out.println("Instruments loaded from file successfully!");
        if (allInstruments.size() == 0) {
            System.out.println("There are no instruments in the store currently");
        }
        return allInstruments;
    }

    private static ArrayList parseGuitars(Scanner scanner) throws AfekaInstrumentsException {
        int numberOfGuitars=Integer.parseInt(scanner.nextLine());
        ArrayList instruments=new ArrayList(numberOfGuitars);
        for (int i=0;i<numberOfGuitars;i++) {
            instruments.add(new Guitar(scanner));
        }
        return instruments;
    }

    private static ArrayList parseBassGuitars(Scanner scanner) throws AfekaInstrumentsException {
        int numberOfBassGuitars=Integer.parseInt(scanner.nextLine());
        ArrayList instruments=new ArrayList(numberOfBassGuitars);
        for (int i=0;i<numberOfBassGuitars;i++) {
            instruments.add(new Bass(scanner));
        }
        return instruments;
    }

    private static ArrayList parseFluet(Scanner scanner) throws AfekaInstrumentsException {
        int numberOfFlues=Integer.parseInt(scanner.nextLine());
        ArrayList instruments=new ArrayList(numberOfFlues);
        for (int i=0;i<numberOfFlues;i++) {
            instruments.add(new Fluet(scanner));
        }
        return instruments;
    }

    private static ArrayList parseSaxophone(Scanner scanner) throws AfekaInstrumentsException {
        int numberOfSaxophones=Integer.parseInt(scanner.nextLine());
        ArrayList instruments=new ArrayList(numberOfSaxophones);
        for (int i=0;i<numberOfSaxophones;i++) {
            instruments.add(new Saxophone(scanner));
        }
        return instruments;
    }
}
