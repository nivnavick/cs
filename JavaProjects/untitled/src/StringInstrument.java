import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class StringInstrument extends Instrument {

    private static final Map<String, String> errMessagesDictionary=createErrorsMap();
    private int stringCount;
    private InstrumentType type;

    public StringInstrument(double price, String manufacturer, int stringCount,String stringInstrumentType) throws AfekaInstrumentsException {
        super(price, manufacturer);
        setType(stringInstrumentType);
        setStringCount(stringCount,InstrumentType.valueOf(stringInstrumentType));
    }

    public StringInstrument(Scanner scanner) throws AfekaInstrumentsException {
        super(scanner);
        int stringsCount=Integer.parseInt(scanner.nextLine());
        setType(scanner.nextLine());
        setStringCount(stringsCount,getType());
    }

    public StringInstrument(Scanner scanner,String type) throws AfekaInstrumentsException {
        super(scanner);
        int stringsCount=Integer.parseInt(scanner.nextLine());
        setType(type);
        setStringCount(stringsCount,getType());
    }

    public void setType(String type) {
        this.type=InstrumentType.valueOf(type);
    }

    public InstrumentType getType() {
       return this.type;
    }

    public int getStringCount() {
        return this.stringCount;
    }

    public void setStringCount(int stringCount,InstrumentType stringInstrumentType) throws AfekaInstrumentsException {
        boolean exit=false;
        String errMessage="";
        String type=stringInstrumentType.name();
        switch (type) {
            case "Electric":
                if(stringCount<6 || stringCount>8) {
                    errMessage=errMessagesDictionary.get(type);
                    exit = true;
                }
                break;
            case "Acoustic":
                if(stringCount!=6) {
                    errMessage+=String.format(errMessagesDictionary.get(type),stringCount);
                    exit = true;
                }
                break;
            case "Classic":
                if(stringCount!=6) {
                    errMessage+=String.format(errMessagesDictionary.get(type),stringCount);
                    exit = true;
                }
                break;
            case "Bass":
                if(stringCount<4 || stringCount>6) {
                    errMessage=errMessagesDictionary.get(type);
                    exit = true;
                }
                break;
        }

        if(exit) {
            throw new AfekaInstrumentsException(errMessage);
        }
        this.stringCount = stringCount;
    }

    private static Map<String, String> createErrorsMap()
    {
        Map<String,String> errorsMapping = new HashMap<>();
        errorsMapping.put("Electric", "Electric number of strings is a number between 6 and 8");
        errorsMapping.put("Acoustic", "Acoustic Guitars have 6 strings, not %d");
        errorsMapping.put("Classic", "Classic Guitars have 6 strings, not %d");
        errorsMapping.put("Bass", "Bass number of strings is a number between 4 and 6");
        return errorsMapping;
    }

    @Override
    public String toString() {
        return String.format("Price:%.2f  Number of strings:%d",super.getPrice(),getStringCount());
    }
}
