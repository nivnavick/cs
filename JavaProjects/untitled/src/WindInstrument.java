import java.util.Scanner;

public class WindInstrument extends Instrument {

    private InstrumentMaterial material;

    public WindInstrument(Scanner scanner) throws AfekaInstrumentsException {
        super(scanner);
        setMaterial(scanner.nextLine());
    }

    public WindInstrument(double price, String manufacturer, String material) throws AfekaInstrumentsException {
        super(price, manufacturer);
        setMaterial(material);
    }

    public void setMaterial(String material) {
        this.material = InstrumentMaterial.valueOf(material);
    }

    public InstrumentMaterial getMaterial() {
      return this.material;
    }

    @Override
    public String toString() {
        return String.format("Price:%7.2f  Made of:%11s",super.getPrice(),getMaterial());
    }
}