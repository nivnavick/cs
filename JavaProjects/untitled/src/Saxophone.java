import java.util.Scanner;

public class Saxophone extends WindInstrument {
    public Saxophone(double price, String manufacturer, String material) throws AfekaInstrumentsException {
        super(price, manufacturer, material);
        verifyMaterial();
    }

    public Saxophone(Scanner scanner) throws AfekaInstrumentsException {
        super(scanner);
        verifyMaterial();
    }

    private void verifyMaterial() throws AfekaInstrumentsException {
        if(getMaterial()!=InstrumentMaterial.Metal) {
            throw new AfekaInstrumentsException("Saxophone material must be metal");
        }
    }

    @Override
    public String toString() {
        return String.format("%-10s %-10s |%s|",
                super.getManufacturer(),
                this.getClass().getSimpleName(),
                super.toString());
    }
}
