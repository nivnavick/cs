import java.util.Scanner;

public class Main3 {
    public static void main(String[] args) {
        int max = 3;
        char[][] ch = {
                {' ', ' ', 'v', 'a',' '},
                {' ', 'a', 'v', ' ',' '},
                {'a', 'b', ' ', ' ','b'},
                {'a', 'a', ' ', 'a',' '},
                {'a', 'a', 'c', ' ',' '},
        };

        int maxW=findMaxWord(ch);
        int min=findMinWord(ch);
        int counterR=searchRows(ch,max);
        int counterC=searchColumns(ch,max);
    }

    private static int findMaxWord(char[][] ch) {
        int length=0;
        int row=0;
        int column=0;
        boolean rows=false;
        for (int i = 0; i < ch.length; i++) {
            int cR = 0;
            int cC= 0;
            for (int j = 0; j < ch.length; j++) {
                if (ch[i][j] != ' ') {
                    cR++;
                }
                if((ch[i][j] == ' ' || j+1>=ch.length) && cR!=0) {
                    if(length<cR) {
                        length=cR;
                        rows=true;
                        row=i;
                        column=j;
                    }
                    cR=0;
                }


                if (ch[j][i] != ' ') {
                    cC++;
                }
                if((ch[j][i] == ' ' || j+1>=ch.length) && cC!=0) {
                    if(length<cC) {
                        length=cC;
                        rows=false;
                        row=j;
                        column=i;
                    }
                    cC=0;
                }
            }
        }
        printWord(rows,row,column,length,ch);
        return length;
    }


    private static int findMinWord(char[][] ch) {
        int length=1111111111;
        int row=0;
        int column=0;
        boolean rows=false;
        for (int i = 0; i < ch.length; i++) {
            int cR = 0;
            int cC = 0;
            for (int j = 0; j < ch.length; j++) {
                if (ch[i][j] != ' ') {
                    cR++;
                }
                if((ch[i][j] == ' ' || j+1>=ch.length) && cR!=0) {
                    if(length>cR) {
                        length=cR;
                        rows=true;
                        row=i;
                        column=j;
                    }
                    cR=0;
                }

                if (ch[j][i] != ' ') {
                    cC++;
                }
                if((ch[j][i] == ' ' || j+1>=ch.length) && cC!=0) {
                    if(length>cC) {
                        length=cC;
                        rows=false;
                        row=j;
                        column=i;
                    }
                    cC=0;
                }

            }
        }
        printWord(rows,row,column,length,ch);
        return length;
    }

    private static void printWord(boolean rows,int row,int column,int length,char[][] ch) {
        if(rows) {
            for(int j=column;j<column+length;j++) {
                System.out.print(ch[row][j]);
            }
        }
        else {
            for(int j=row-(length-1);j<length+(row-(length-1));j++) {
                System.out.print(ch[j][column]);
            }
        }
    }




    private static int searchColumns(char[][] ch, int max) {
        int counter = 0;
        for (int i = 0; i < ch.length; i++) {
            int c = 0;
            for (int j = 0; j < ch.length; j++) {
                if ((ch[j][i] == ' ' && c != max) || (c == max && ch[j][i] != ' ')) {
                    c = 0;
                } else if (c == max && (ch[j][i] == ' ' || j + 1 >= ch.length)) {
                    counter++;
                    c = 0;
                } else if (ch[j][i] != ' ') {
                    c++;
                }
            }

            if (c == 3) {
                counter++;
            }
        }
        return counter;
    }

    private static int searchRows(char[][] ch, int max) {
        int counter = 0;
        for (int i = 0; i < ch.length; i++) {
            int c = 0;
            for (int j = 0; j < ch.length; j++) {
                if ((ch[i][j] == ' ' && c != max) || (c == max && ch[i][j] != ' ')) {
                    c = 0;
                } else if (c == max && (ch[i][j] == ' ' || j + 1 >= ch.length)) {
                    counter++;
                    c = 0;
                } else if (ch[i][j] != ' ') {
                    c++;
                }
            }

            if (c == 3) {
                counter++;
            }
        }
        return counter;
    }
}
