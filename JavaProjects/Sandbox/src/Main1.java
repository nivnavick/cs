import java.text.DecimalFormat;
import java.util.Scanner;

public class Main1 {

    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        double[][] jagged=createJaggedArray(scanner.nextInt());
        printMat(jagged);
        int index=searchRowJaggedArray(jagged,scanner.nextDouble());
        if(index!=-1) {
            printJaggedArray(jagged,index);
        }
    }


    private static double[] generateRandomArray(int index) {
        double[] array=new double[index];
        String decimalFormat;
        for(int i=0;i<index;i++) {
                    decimalFormat=index%2==0?
                    new DecimalFormat("#.##").format(Math.random()):
                            new DecimalFormat("#").format(Math.random()*10);
            array[i]=Double.parseDouble(decimalFormat);
        }
        return array;
    }

    private static double[][] createJaggedArray(int rows) {
        double[][] jaggedArray = new double[rows][];
        for(int i=0;i<rows;i++) {
            jaggedArray[i]=generateRandomArray(i+1);
            bubbleSort(jaggedArray[i]);
        }
        return  jaggedArray;
    }

    private static void printJaggedArray(double[][] jagged,int index) {
        for (double number : jagged[index]) {
            System.out.print(number + " ");
        }
    }

    private static int searchRowJaggedArray(double[][] jagged,double number) {
        for (int i = 0; i < jagged.length; i++) {
            double[] array = jagged[i];
            int index = binarySearch(array, number);
            if (index != -1) {
                return i;
            }
        }
        return -1;
    }

    private static int binarySearch(double a[], double val) {
        int low = 0, high = a.length - 1, middle;
        while (low <= high) {
            middle = (low + high) / 2;
            if (val == a[middle])
                return middle;
            else if (val < a[middle])
                high = middle - 1;
            else
                low = middle + 1;
        }
        return -1;
    }

    private static void bubbleSort(double[] arr) {
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1])
                    swap(arr, j, j + 1);
            }
        }
    }

    private static void swap(double[] arr, int i, int j) {
        //Swap cells (helper method of bubble sort)
        double temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void printMat(double[][] mat) {
        for (double[] array : mat) {
            for (double anArray : array) {
                System.out.printf("%4f ", anArray);
            }
            System.out.println();
        }
    }
}
