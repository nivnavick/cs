
public class Main {

    private final static int EXCLUDE_NUMBER = 0;

    public static void main(String[] args) {

        String a="AAC";
        String b="ABC";

       int x= a.compareToIgnoreCase(b);



      //  int x=dec2Bin(8);
        int[] set = new int[] { 1, 2, 3, 4 };
        int[] subset = new int[set.length];
        System.out.print("Array: ");
        printArray(set);

        System.out.println("Subsets of array: ");
        subset(set, subset, 0);
        System.out.println();
    }


    private static void printArray(int[] arr) {
        System.out.print('{');
        printArray(arr, 0, true);
        System.out.println('}');
    }

    private static void printArray(int[] arr, int index, boolean isFirst) {
        if (index >= arr.length) {
            return;
        }
        if (arr[index] != EXCLUDE_NUMBER) {
            if (isFirst) {
                System.out.printf("%d", arr[index]);
            } else {
                System.out.printf(", %d", arr[index]);
            }
            isFirst = false;

        }
        printArray(arr, index + 1, isFirst);
    }

    private static void subset(int[] set, int[] subset, int idx) {
        if (idx == set.length) {
            printArray(subset);
            return;
        }

        // Complete the recursion
    }


    public  static  int  factorial(int n) {
        if (n <= 1)
        return 1;
        return factorial(n-1)*n;
    }


    /* Question 3 */
    private static void specialPrint(String str, char delimeter) {
        if (str == null || str.length() == 0) {
            return;
        }
        System.out.print(String.format("%c%c",str.charAt(0),delimeter));
        specialPrint(str.substring(1),delimeter);
    }


    /* Question 1.1 */
    private static int dec2Bin(int num) {
        if (num == 0) {
            return 0;
        }
        // Complete the recursion
        return (num % 2 + 10 * dec2Bin(num / 2));
    }

    private static int bin2Dec(int num) {
        if (num == 0) {
            return 0;
        }
        return (num % 10 + bin2Dec(num / 10) * 2);
    }

    /* Question 1.3 */
    private static int hex2Dec(String str) {

        int decimal = 0;
        str = str.toUpperCase();
        int length = str.length();
        if (str == null || str.length() == 0) {
            return 0;
        }

        char ch = str.charAt(0);
        int digit = hexaMap[ch];
        String substring = str.substring(1);
        return digit * (int) Math.pow(16, length - 1) + hex2Dec(substring);

        // Complete the recursion
    }

    private static String dec2Hex(int n) {
        int temp = n % 16;
        char digit = hexaMap[temp];

        if ((n - temp) / 16 == 0)
            return String.valueOf(digit);
        if (n > 0)
            return dec2Hex((n - temp) / 16) + digit;

        return "";
    }

    private static char[] hexaMap = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };


    private static void printTriangle(int triangleHeight,int topVertexRow,int topVertexColumn) {
        int rowLength=(2 * triangleHeight) - 1;
        char[][] x=new char[20][20];
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[0].length; j++)
                x[i][j]='@';
        }

        for (int i = triangleHeight; i > 0; i--)
        {
            for (int j = 0; j < rowLength; j++)
            {
                if ((topVertexRow + i >= 0) && (topVertexRow + i < 20) && (topVertexColumn - i + j >= 0) && (topVertexColumn - i + j < 20)) {
                    x[topVertexRow + i][topVertexColumn - i + j] = ' ';
                }
            }
            rowLength -= 2;
        }
        printMat(x);
    }

    private static int sumMatrixMinor(int[][] matrix,int row,int column) {
        int sum=0;
        for(int i=0;i<matrix.length;i++) {
            for(int j=0;j<matrix[0].length;j++) {
                if(i!=row && j!=column){
                    sum+=matrix[i][j];
                }
            }
        }
        return sum;
    }

    private static void shiftArray(int []arr,int shift) {
        for (int i = 0; i < shift; i++) {
            for (int j = arr.length - 1; j > 0; j--) {
                int temp = arr[j];
                arr[j] = arr[j - 1];
                arr[j - 1] = temp;
            }
        }
    }



    public static void printMat(char[][] mat) {
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++)
                System.out.printf("%4c ", mat[i][j]);
            System.out.println();
        }
    }
}
