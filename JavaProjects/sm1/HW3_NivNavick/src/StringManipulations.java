public class StringManipulations {
    //Replace first char to capital
    public static String stringToFirstUpper(String text) {
        return Character.toUpperCase(text.charAt(0)) + text.substring(1, text.length()).toLowerCase();
    }

    public static String removeDoubleSpaces(String text) {
        return text.replaceAll("\\s+", " ");
    }
}
