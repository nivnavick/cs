public class AlbumSet {

    // Behaviours
    private String owner;
    private Album[] albums;
    private int numAlbums;
    private final int MAX_ALBUMS=20;

    // Constructor
    public AlbumSet(String owner) {
        this.owner=StringManipulations.stringToFirstUpper(owner);
        this.albums = new Album[MAX_ALBUMS];
        this.numAlbums = 0;
    }

    // getters
    public String getOwner() {
        return this.owner;
    }

    public int getNumAlbums() {
        return this.numAlbums;
    }

    public Album getOneAlbumByIndex(int index) {
        return this.albums[index];
    }

    // special setter: adding song to album
    public void addSongToAlbum(String albumName, String songName, String artistName, int minutes, int seconds) {
        Song song = new Song(songName, artistName, minutes, seconds);
        if (isAlbumExists(albumName)) {
            getOneAlbumByIndex(getAlbumIndex(albumName)).addSong(song);
        } else {
            addNewAlbum(albumName);
            this.albums[this.numAlbums-1].addSong(song);
        }
    }

    private void addNewAlbum(String albumName) {
        this.albums[this.numAlbums++] = new Album(albumName);
    }

    // special Methods check if album exists
    public boolean isAlbumExists(String albumName) {
       return getAlbumIndex(albumName)!=-1;
    }

    public int getAlbumIndex(String albumName) {
        for (int i = 0; i < getNumAlbums(); i++) {
            if (this.albums[i].getAlbumName().equalsIgnoreCase(albumName)) {
                return i;
            }
        }
        return -1;
    }

    //Sorting by ABC
    public void sortByAlbumsName() {
        for (int i = this.numAlbums - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if(albums[j].getAlbumName().compareToIgnoreCase(this.albums[j + 1].getAlbumName())>0) {
                    swap(this.albums, j, j + 1);
                }
            }
        }
    }

    private static void swap(Album[] albums, int i, int j) {
        //Swap cells (helper method of bubble sort)
        Album temp = albums[i];
        albums[i] = albums[j];
        albums[j] = temp;
    }

    public String toShortString() {
        StringBuilder sb=new StringBuilder();
        sb.append("Number of Albums:"+this.numAlbums+"\n");
        for (int i = 0; i < this.numAlbums; i++) {
            sb.append(this.albums[i].toShortString()+"\n");
        }
        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.numAlbums; i++) {
            sb.append(String.format("Album no' %d\n=============\n", i + 1));
            this.albums[i].sortByArtist();
            sb.append(this.albums[i].toString() + "\n\n");
        }
        return sb.toString();
    }

}