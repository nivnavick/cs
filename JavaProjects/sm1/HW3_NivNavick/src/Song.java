public class Song {
    // class behaviours
    private String songName = "";
    private String artistName = "";
    private int songLength = 0;

    //constructors
    public Song(String songName, String artistName, int minutes, int seconds) {
        setSongName(songName);
        setArtistName(artistName);
        setSongLength(minutes,seconds);
    }

    public Song(String songName, String artistName, int seconds) {
        setSongName(songName);
        setArtistName(artistName);
        setSongLength(seconds);
    }

    // getters
    public String getSongName() {
        return this.songName;
    }

    public String getArtistName() {
        return this.artistName;
    }

    public int getSongLength() {
        return this.songLength;
    }

    // setters
    public void setSongName(String songName) {
        this.songName = StringManipulations.stringToFirstUpper(songName.trim());
    }

    public void setArtistName(String artistName) {
        this.artistName = StringManipulations.stringToFirstUpper(artistName.trim());
    }

    public void setSongLength(int minutes, int seconds) {
        this.songLength = 60 * minutes + seconds;
    }

    public void setSongLength(int seconds) {
        this.songLength = seconds;
    }

    // special Methods
    public boolean isSongEqaul(String songName) {
      return this.songName.equalsIgnoreCase(songName);
    }

    public boolean isArtistEqual(String artistName) {
        return this.artistName.equalsIgnoreCase(artistName);
    }

    // to String
    public String toString() {
        return "Song Name: " + this.songName + ", Artist Name: " + this.artistName
                + ", Song Length: " + this.songLength + " seconds";
    }
}