//	Student Name: Niv Navick,
// 	Student ID: 312164411

import java.io.*;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Ex03_Main {

    public static void main(String[] args) throws FileNotFoundException {

        // main program variables
        // ----------------------
        AlbumSet myAlbums = new AlbumSet("niv");

        // phase A
        //-----------
        // getting file name and open it
        String songsInputFileName = JOptionPane.showInputDialog(null, "Please enter input file name (full path)");
        // reading input file and creating the albums
        parseInputFileToAlbumSet(myAlbums, songsInputFileName);

        // phase B
        // -------
        // album and song ordering
        myAlbums.sortByAlbumsName();
        for (int i = 0; i < myAlbums.getNumAlbums(); i++) {
            myAlbums.getOneAlbumByIndex(i).sortByArtist();
        }

        // phase C
        // -------
        // statistical information
        System.out.println(myAlbums.toShortString());

        // phase D
        // -------
        // searching for a song
        Boolean searchForSong = JOptionPane.showConfirmDialog(null,
                "Do you want to search a song?", "", JOptionPane.YES_NO_OPTION) == 0;

        if (searchForSong) {
            String songToSearch = JOptionPane.showInputDialog(null, "Please enter a song name to search");
            System.out.println(searchSong(myAlbums, songToSearch));
        }
        // phase E
        // ----------
        // getting output file name and write it...
        String songsOutputFileName = JOptionPane.showInputDialog(null, "Please enter output file name (full path");
        writeAlbumsDataToFile(myAlbums, songsOutputFileName);
    }

    private static String searchSong(AlbumSet albumSet, String songName) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < albumSet.getNumAlbums(); i++) {
            Album currentAlbum = albumSet.getOneAlbumByIndex(i);
            int index = currentAlbum.isSongExist(songName);
            if (index != -1) {
                sb.append(String.format("Song: %s exist as album: %s as song number: %d\n", songName, currentAlbum.getAlbumName(), index + 1));
            }
        }
        if (sb.length() == 0) {
            sb.append(String.format("Song:%s does not exist", songName));
        }
        return sb.toString();
    }

    private static void writeAlbumsDataToFile(AlbumSet albumSet, String filePath) throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter(filePath);
        printWriter.print(albumSet.toString());
        printWriter.close();
    }

    private static void parseInputFileToAlbumSet(AlbumSet albumSet, String filePath) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(filePath));
        while (scanner.hasNext()) {
            addSongBasedStringLine(albumSet,scanner.nextLine());
        }
        scanner.close();
    }

    public static void addSongBasedStringLine(AlbumSet albumSet,String line) {
        final int SONG_NAME_INDEX=0;
        final int ARTIST_NAME_INDEX=1;
        final int SONG_DURATION_INDEX=2;
        final int ALBUM_NAME_INDEX=3;
        final int MINUTES_DURATION_INDEX=0;
        final int SECONDS_DURATION_INDEX=1;

        String[] songMetadata = StringManipulations.removeDoubleSpaces(line).split(";");
        String songName= songMetadata[SONG_NAME_INDEX];
        String artistName= songMetadata[ARTIST_NAME_INDEX];
        String[] duration = songMetadata[SONG_DURATION_INDEX].split(":");
        String albumName=songMetadata[ALBUM_NAME_INDEX].trim();
        int minutesDuration= Integer.parseInt(duration[MINUTES_DURATION_INDEX].trim());
        int secondsDuration=  Integer.parseInt(duration[SECONDS_DURATION_INDEX].trim());
        albumSet.addSongToAlbum(albumName, songName, artistName, minutesDuration ,secondsDuration);
    }
}
