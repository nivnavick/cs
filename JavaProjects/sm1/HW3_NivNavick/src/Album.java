public class Album {
    // class behaviours
    private String albumName;
    private Song[] songs;
    private int totalAlbumLength;
    private int numSongs;
    private final int MAX_SONGS=20;

    // constructor
    public Album(String albumName) {
        setAlbumName(albumName);
        this.songs = new Song[MAX_SONGS];
    }

    // getters
    public String getAlbumName() {
        return this.albumName;
    }

    public Song[] getAlbumSongs() {
        return this.songs;
    }

    public int getAlbumLength() {
        return this.totalAlbumLength;
    }

    public int getNumSongs() {
        return this.numSongs;
    }

    // setters
    public void setAlbumName(String albumName) {
        this.albumName =   StringManipulations.stringToFirstUpper(albumName.trim());
    }

    // special setters: adding new song
    public void addSong(Song newSong) {
        this.songs[numSongs++] = newSong;
       this. totalAlbumLength += newSong.getSongLength();
    }

    public void addSong(String songName, String artistName, int minutes, int seconds) {
        addSong(new Song(songName, artistName, minutes, seconds));
    }

    // special Methods
    public int isSongExist(String songName) {
        for (int i = 0; i < this.numSongs; i++) {
            if (this.songs[i].getSongName().equalsIgnoreCase(songName)) {
                return i;
            }
        }
        return -1;
    }

    //Sorting by ABC
    public void sortByArtist() {
        for (int i = numSongs - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if(this.songs[j].getArtistName().compareToIgnoreCase(this.songs[j + 1].getArtistName())>0) {
                    swap(this.songs, j, j + 1);
                }
            }
        }

        sortByArtistSongsLength();
    }

    //Sorting by same artist song length,if no multi artist's songs=> no sorting
    private void sortByArtistSongsLength() {
        for (int i = numSongs - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (this.songs[j].isArtistEqual(this.songs[j+1].getArtistName()) && this.songs[j].getSongLength()>this.songs[j+1].getSongLength())
                    swap(this.songs, j, j + 1);
            }
        }
    }

    private static void swap(Song[] albums, int i, int j) {
        //Swap cells (helper method of bubble sort)
        Song temp = albums[i];
        albums[i] = albums[j];
        albums[j] = temp;
    }

    public String toShortString() {
        return "Album Name: " + this.albumName
                + ", number of Songs: " + this.numSongs
                + ", Album Length (in seconds): " + this.totalAlbumLength;
    }

    // toString
    public String toString() {
        String str = toShortString();
        for (int i = 0; i < this.numSongs; i++) {
            str += "\n" + this.songs[i];
        }
        return str;
    }

}