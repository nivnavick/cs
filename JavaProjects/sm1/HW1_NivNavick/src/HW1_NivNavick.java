//Niv Navick
//312164411
import java.util.Random;
import javax.swing.JOptionPane;
public class HW1_NivNavick {
    public static void main(String[] args) {
        //Members
        final String USERNAME_MESSAGE_FORMAT = "Please enter full name for player #%s";
        final String GUESS_NUMBERS_MESSAGE_FORMAT = "%d number/s guessed correctly";
        final String GUESS_NUMBERS_ITERATION_MESSAGE_FORMAT = "Player #%d %s,\nPlease enter your %d guess";
        final String WINNER_MESSAGE = "congratulations\nPlayer #%d %s won\nDo you want to start a new game?";
        final String PRACTICE_MESSAGE_FORMAT = "How many secret numbers do you want to show:\nPress 1,2,3 or cancel for none";
        final String ERROR_PRACTICE_MESSAGE = "Please enter 1,2,3 only";
        final String ERROR_DUPLICATED_GUESS_INPUT_MESSAGE = "Duplicated guesses cannot be written";
        final String NEW_GAME_DIALOG_HEADER="Select an option";
        final String ERROR_DIALOG_HEADER="Error";
        final String PRACTICE_MODE_NUMBERS_BASE="\nSecret Number/s:%d";
        final int NUMBER_OF_GUESSES = 3;
        final int MAX_RANDOM_NUMBER=5;
        final int FIRST_PLAYER_NUMBER_TO_PLAY = 1;
        Boolean userEndedGame = false;

        while (!userEndedGame) {
            //input from user of usernames
            String firstPlayerName = JOptionPane.showInputDialog(String.format(USERNAME_MESSAGE_FORMAT, 1));
            String secondPlayerName = JOptionPane.showInputDialog(String.format(USERNAME_MESSAGE_FORMAT, 2));
            //Generate random numbers
            Random random = new Random();
            int firstRandomNumber;
            int secondRandomNumber;
            int thirdRandomNumber;
            do {
                firstRandomNumber = random.nextInt(MAX_RANDOM_NUMBER) + 1;
                secondRandomNumber = random.nextInt(MAX_RANDOM_NUMBER) + 1;
                thirdRandomNumber = random.nextInt(MAX_RANDOM_NUMBER) + 1;
            } while (firstRandomNumber == secondRandomNumber || firstRandomNumber == thirdRandomNumber || secondRandomNumber == thirdRandomNumber);
            //Input from user if to enter practice mode
            Boolean practiceInputValid= false;
            String practiceModeInput;
            do {
                practiceModeInput = JOptionPane.showInputDialog(null, PRACTICE_MESSAGE_FORMAT);
                if (practiceModeInput != null) {
                    switch (practiceModeInput) {
                        case "1":
                            practiceInputValid=true;
                            System.out.printf(PRACTICE_MODE_NUMBERS_BASE,firstRandomNumber);
                            break;
                        case "2":
                            practiceInputValid=true;
                            System.out.printf(PRACTICE_MODE_NUMBERS_BASE+"|%d",firstRandomNumber,secondRandomNumber);
                            break;
                        case "3":
                            practiceInputValid=true;
                            System.out.printf(PRACTICE_MODE_NUMBERS_BASE+"|%d|%d",firstRandomNumber,secondRandomNumber,thirdRandomNumber);
                            break;
                        default:
                            JOptionPane.showMessageDialog(null, ERROR_PRACTICE_MESSAGE,
                                    ERROR_DIALOG_HEADER, JOptionPane.ERROR_MESSAGE);
                            break;
                    }
                }
            } while (!practiceInputValid && practiceModeInput != null);
            //Game starts here, in "while", checking if one of the users has won, in "for" , iterating through each player guess
            int playerNumber = FIRST_PLAYER_NUMBER_TO_PLAY;
            String currentPlayerName = null;
            Boolean gameEndedByWin = false;
            while (!gameEndedByWin) {
                currentPlayerName = playerNumber == 1 ? firstPlayerName : secondPlayerName;
                int correctGuessCount = 0;
                int firstGuess = -1;
                int secondGuess = -2;
                int thirdGuess = -3;
                for (int currentGuessNumber = 1; currentGuessNumber <= NUMBER_OF_GUESSES; currentGuessNumber++) {
                    int userDuplicatedInputCount = 0;
                    int userGuess;
                    do {
                        if (userDuplicatedInputCount > 0) {
                            JOptionPane.showMessageDialog(null, ERROR_DUPLICATED_GUESS_INPUT_MESSAGE,
                                    ERROR_DIALOG_HEADER, JOptionPane.ERROR_MESSAGE);
                        }
                        userGuess = Integer.parseInt( JOptionPane.showInputDialog(String.format(GUESS_NUMBERS_ITERATION_MESSAGE_FORMAT,
                                        playerNumber, currentPlayerName, currentGuessNumber)));

                        switch (currentGuessNumber) {
                            case 1:
                                firstGuess = userGuess;
                                break;
                            case 2:
                                secondGuess = userGuess;
                                break;
                            case 3:
                                thirdGuess = userGuess;
                                break;
                        }
                        userDuplicatedInputCount++;
                    } while (firstGuess == secondGuess || firstGuess == thirdGuess || secondGuess == thirdGuess);

                    if (userGuess == firstRandomNumber || userGuess == secondRandomNumber || userGuess == thirdRandomNumber) {
                        correctGuessCount++;
                    }
                }
                JOptionPane.showMessageDialog(null,String.format(GUESS_NUMBERS_MESSAGE_FORMAT, correctGuessCount));
                gameEndedByWin=correctGuessCount == NUMBER_OF_GUESSES;
                playerNumber=playerNumber==1?2:1;
            }
            userEndedGame=JOptionPane.showConfirmDialog(null,
                    String.format(WINNER_MESSAGE, playerNumber, currentPlayerName),
                    NEW_GAME_DIALOG_HEADER,JOptionPane.YES_NO_OPTION) == 1;
        }
    }
}
