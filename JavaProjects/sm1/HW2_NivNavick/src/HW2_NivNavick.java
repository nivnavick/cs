//Niv Navick
//312164411

import javax.swing.JOptionPane;
import java.util.Random;

public class HW2_NivNavick {
    private static String HEX_DIGITS = "0123456789ABCDEF";
    public static void main(String[] args) {
        task4();
        task7();
        task8();
    }

    private static void task4() {
        int[][] multiplyMatrix = multiplyMatrix(
                buildMatrix(integerInput(1), integerInput(2)),
                buildMatrix(integerInput(3), integerInput(4)));
        showMatrixOnScreen(multiplyMatrix);
    }

    private static void task7() {
        char[] hex = buildHexArray(3);
        int decimalFromHex = convertFromHexToDecimal(hex);
        int binary = generateRandomBinary(4);
        int decimalFromBinary = convertBinaryToDecimal(binary);
        JOptionPane.showMessageDialog(null,
                String.format("(Bin)%s=(Dec)%d\n(Hex)%s=(Dec)%d", binary, decimalFromBinary, String.valueOf(hex), decimalFromHex));
    }

    private static void task8() {
        JOptionPane.showMessageDialog(null, "Please enter size of array");
        int[] array = buildArray(Integer.parseInt(JOptionPane.showInputDialog(null, "Enter size")));
        bubbleSort(array);
        showArrayOnScreen(array);
        int numberToFind = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter number between 1-127"));
        int binarySearchIndex = binarySearch(array, numberToFind);
        if (binarySearchIndex != -1) {
            String hexValue = convertFromDecimalToHex(numberToFind);
            int binaryValue = convertDecimalToBinary(numberToFind);
            JOptionPane.showMessageDialog(null,
                    String.format("Index:%d\n%d(Dec)=%d(Bin)=%s(Hex)", binarySearchIndex, numberToFind, binaryValue, hexValue));
        }
    }

    private static int integerInput(int iterationNumber) {
        //Returns integer from input dialog
        return Integer.parseInt(JOptionPane
                .showInputDialog(null, String.format("Enter number %d", iterationNumber)));
    }

    private static int[][] transposeMatrix(int[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;
        int[][] trasposedMatrix = new int[n][m];
        for (int x = 0; x < n; x++) {
            for (int y = 0; y < m; y++) {
                //Swapping item from column to row
                trasposedMatrix[x][y] = matrix[y][x];
            }
        }
        return trasposedMatrix;
    }

    private static int[][] multiplyMatrix(int[][] matrixA, int[][] matrixB) {
        if (matrixA[0].length != matrixB.length) {
            throw new ArithmeticException("Cannot multiply matrix A by B");
        }
        int[][] result = new int[matrixA.length][matrixB[0].length];
        for (int i = 0; i < matrixA.length; i++) {
            for (int j = 0; j < matrixB[0].length; j++) {
                for (int k = 0; k < matrixA[0].length; k++) {
                    result[i][j] += matrixA[i][k] * matrixB[k][j];
                }
            }
        }
        return result;
    }

    private static int[][] buildMatrix(int rows, int columns) {
        int[][] randomNumbersMatrix = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            randomNumbersMatrix[i] = buildArray(columns);
            bubbleSort(randomNumbersMatrix[i]);
        }
        return randomNumbersMatrix;
    }

    private static int[] buildArray(int arrayLength) {
        final int randomMax = 127;
        final int randomMin = 1;
        Random random = new Random();
        int[] randomNumbersArray = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            randomNumbersArray[i] = random.nextInt(randomMax + 1) - randomMin;
        }
        return randomNumbersArray;
    }

    private static char[] buildHexArray(int length) {
        Random random = new Random();
        char[] hexArray = new char[length];
        for (int i = 0; i < length; i++) {
            hexArray[i] = HEX_DIGITS.charAt(random.nextInt(HEX_DIGITS.length()));
        }
        return hexArray;
    }

    private static int convertFromHexToDecimal(char[] hexValue) {
        int decimalValue = 0;
        for (char value : hexValue) {
            int digit = HEX_DIGITS.indexOf(Character.toUpperCase(value));
            decimalValue = (16 * decimalValue) + digit;
        }
        return decimalValue;
    }

    private static int convertDecimalToBinary(int num) {
        int index = 0;
        int binaryNum = 0;
        while (num > 0) {
            binaryNum += (num % 2) * Math.pow(10, index++);
            num /= 2;
        }
        return binaryNum;
    }

    private static int convertBinaryToDecimal(int binaryNumber) {
        int decimal = 0;
        int power = 0;
        while (binaryNumber != 0) {
            int temp = binaryNumber % 10;
            //Converting every binary digit to decimal
            decimal += temp * Math.pow(2, power++);
            binaryNumber /= 10;
        }
        return decimal;
    }

    private static String convertFromDecimalToHex(int num) {
        int temp;
        StringBuilder hex = new StringBuilder();
        while (num > 0) {
            temp = num % 16;
            //Converting to hex using hex digits pattern
            hex.insert(0, HEX_DIGITS.charAt(temp));
            num /= 16;
        }
        return hex.toString();
    }

    private static int generateRandomBinary(int length) {
        int binaryNumber = 0;
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            //Summing binary
            binaryNumber += random.nextInt(2) * Math.pow(10, i);
        }
        return binaryNumber;
    }

    private static void swap(int[] arr, int i, int j) {
        //Swap cells (helper method of bubble sort)
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private static void bubbleSort(int[] arr) {
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1])
                    swap(arr, j, j + 1);
            }
        }
    }

    private static int binarySearch(int a[], int val) {
        int low = 0, high = a.length - 1, middle;
        while (low <= high) {
            middle = (low + high) / 2;
            if (val == a[middle])
                return middle;
            else if (val < a[middle])
                high = middle - 1;
            else
                low = middle + 1;
        }
        return -1;
    }

    private static void showMatrixOnScreen(int[][] matrix) {
        //Generating html format message of 2 dimensions array
        StringBuilder builder = new StringBuilder(matrix.length);
        builder.append("<html><table style='background:white'>");
        for (int[] aMatrix : matrix) {
            builder.append("<tr>");
            for (int j = 0; j < matrix[0].length; j++) {
                builder.append(String.format("<td>%d</td>", aMatrix[j]));
            }
            builder.append("</tr>");
        }
        builder.append("</table></html>");
        JOptionPane.showMessageDialog(null, builder.toString());
    }

    private static void showArrayOnScreen(int[] array) {
        //Generating html format message of 1 dimension array
        StringBuilder builder = new StringBuilder(array.length);
        builder.append("<html><table style='background:white'>");
        for (int arrayItem : array) {
            builder.append(String.format("<td>%d</td>", arrayItem));
        }
        builder.append("</tr>");
        builder.append("</table></html>");
        JOptionPane.showMessageDialog(null, builder.toString());
    }
}
