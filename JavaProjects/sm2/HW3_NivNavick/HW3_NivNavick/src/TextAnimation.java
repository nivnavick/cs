//Niv Navick
//312164411
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class TextAnimation {
	final String TEXT_CONTENT = "Afeka Instruments Music Store $$ ON SALE!!! $$$ Guitars. Basses, Flutes, Saxophones, and more!";
	Text text = new Text();

	public TextAnimation() {
		setTextStyle();
		generatePathTransition();
		setClock();
	}
	
	private void setTextStyle() {
		text.setFill(Color.RED);
		text.setFont(Font.font(text.getFont().getName(), FontWeight.BOLD, 14));
	}

	private void generatePathTransition() {
		Line line1 = new Line();
		line1.setEndX(650);
		line1.setStartX(350);
		PathTransition pathTransition = new PathTransition();
		pathTransition.setDuration(Duration.seconds(10));
		pathTransition.setPath(line1);
		pathTransition.setNode(text);
		pathTransition.setCycleCount(Timeline.INDEFINITE);
		pathTransition.setAutoReverse(true);
		pathTransition.play();
	}

	private void setClock() {
		final Timeline digitalTime = new Timeline(new KeyFrame(Duration.seconds(0), new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
				Calendar cal = Calendar.getInstance();
				String timeString = dateFormat.format(cal.getTime());
				text.setText(timeString + " " + TEXT_CONTENT);
			}
		}), new KeyFrame(Duration.seconds(1)));

		digitalTime.setCycleCount(Animation.INDEFINITE);
		digitalTime.play();
	}

	public Shape getTextAsAnimation() {
		return text;
	}
}
