//Niv Navick
//312164411
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AddBassView extends AddInstrumentView {

	private static final String COMBOBOX_TITLE = "Bass";
	public AddBassView(Stage primaryStage, Stage newWindow) {
		super(primaryStage, newWindow,COMBOBOX_TITLE);
	}

	@Override
	public Scene getScene() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(8);
		grid.setHgap(10);

		VBox mainBox = new VBox();
		mainBox.setAlignment(Pos.CENTER);
		mainBox.getChildren().add(super.getCombobox());

		Label brandLabel = new Label("Brand:");
		GridPane.setConstraints(brandLabel, 0, 1);

		TextField brandBox = new TextField();
		brandBox.setPromptText("Ex: Fender Jazz");
		GridPane.setConstraints(brandBox, 1, 1);

		Label priceLabel = new Label("Price:");
		GridPane.setConstraints(priceLabel, 0, 2);

		TextField priceBox = new TextField();
		priceBox.setPromptText("Ex: 7500");
		GridPane.setConstraints(priceBox, 1, 2);

		Label numOfStringLabel = new Label("Number of string:");
		GridPane.setConstraints(numOfStringLabel, 0, 3);

		TextField stringsBox = new TextField();
		stringsBox.setPromptText("Ex: 4");
		GridPane.setConstraints(stringsBox, 1, 3);

		Label fretlessLabel = new Label("Fretless:");
		GridPane.setConstraints(fretlessLabel, 0, 4);

		CheckBox fretlessCheckbox = new CheckBox();
		GridPane.setConstraints(fretlessCheckbox, 1, 4);

		Button addButton = new Button("Add");
		addButton.setOnAction((event) -> {

			try {
				super.verifyFieldData(brandBox, "Brand");
				super.verifyFieldData(priceBox, "Price");
				super.verifyFieldData(stringsBox, "Number of strings");
				AfekaInstruments.getInstance()
						.addInstrument(new Bass(brandBox.getText(), Double.parseDouble(priceBox.getText()),
								Integer.parseInt(stringsBox.getText()), fretlessCheckbox.isSelected()));
				super.close();
			} catch (Exception ex) {
				super.showError(ex);
			}
		});

		GridPane.setConstraints(mainBox, 0, 0);
		GridPane.setColumnSpan(addButton, 2);
		GridPane.setConstraints(addButton, 1, 5);

		grid.getChildren().addAll(brandLabel, brandBox, priceLabel, numOfStringLabel, stringsBox, fretlessLabel,
				fretlessCheckbox, priceBox, addButton);

		mainBox.getChildren().add(grid);
		return new Scene(mainBox, getWindow().getHeight(), getWindow().getHeight());

	}
}
