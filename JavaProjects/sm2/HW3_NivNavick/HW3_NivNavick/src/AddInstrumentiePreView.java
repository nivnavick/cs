//Niv Navick
//312164411
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class AddInstrumentiePreView {

	private static final String WINDOW_TITLE = "Add Instrument";
	private Stage window;
	private Stage newWindow;

	public AddInstrumentiePreView(Stage primaryStage) {
		window = primaryStage;
		InitizlizeNewWindow();
	}

	private void InitizlizeNewWindow() {
		newWindow = new Stage();
		AddInstrumentComobox combobox = new AddInstrumentComobox(window, newWindow);
		combobox.setAction(new Runnable() {
			@Override
			public void run() {
				newWindow.setScene(combobox.getCurrentView().getScene());
			}
		});
		StackPane secondaryLayout = new StackPane();
		secondaryLayout.getChildren().add(combobox.getField());

		Scene secondScene = new Scene(secondaryLayout, window.getHeight(), window.getHeight());

		newWindow.setTitle(WINDOW_TITLE);
		newWindow.setScene(secondScene);
		newWindow.setX(window.getX() + 200);
		newWindow.setY(window.getY() + 100);

		newWindow.show();
	}
}
