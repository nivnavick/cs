//Niv Navick
//312164411
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javafx.scene.Node;
import javafx.scene.control.Button;

public class ActionButtons {
	HashMap<String, Button> buttons = new HashMap<>();

	public ActionButtons () {
	}
	
	public void createButton(String buttonName,String title,Runnable callback) {
		createButton(buttonName,title);
		setButtonAction(buttonName,callback);
	}
	
	public void createButton(String buttonName) {
		buttons.put(buttonName, new Button());
	}
	
	public void createButton(String buttonName,String title) {
		buttons.put(buttonName, new Button(title));
	}
	
	public Button getButton(String buttonName) {
		return buttons.get(buttonName);
	}
	
	public void setButtonAction(String buttonName,Runnable callback) {
		getButton(buttonName).setOnAction((event) -> {
			callback.run();
		});
	}

	public ArrayList<Node> getAll() {
		ArrayList<Node> nodes=new ArrayList<Node>();
		for(Entry<String,Button> s:buttons.entrySet()) {
			nodes.add(s.getValue());
		}
		return nodes;
	}
}
