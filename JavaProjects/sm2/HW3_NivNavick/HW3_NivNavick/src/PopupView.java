//Niv Navick
//312164411
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import java.util.*;

public class PopupView  {

	public PopupView(String title,String message,AlertType type,Runnable callback) {
		    Alert alert = new Alert(type);
		    alert.setTitle(title);
		    alert.setContentText(message);
		    
		    Optional<ButtonType> result = alert.showAndWait();
		    ButtonType button = result.orElse(ButtonType.CANCEL);

		    if (button == ButtonType.OK) {
		        runCallback(callback);
		    }
	}
	
	private void runCallback(Runnable callback)
	{
	    callback.run();
	}
}
