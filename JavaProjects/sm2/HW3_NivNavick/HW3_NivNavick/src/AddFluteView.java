//Niv Navick
//312164411
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AddFluteView extends AddInstrumentView {

	private static final String COMBOBOX_TITLE = "Flute";
	final private String[] comboboxOptoions = new String[] { COMBOBOX_TITLE, "Recorder", "Bass" };
	final private String[] comboboxOptoionsMaterials = new String[] { "Wood", "Metal", "Plastic" };

	public AddFluteView(Stage primaryStage, Stage newWindow) {
		super(primaryStage, newWindow,COMBOBOX_TITLE);
	}

	@Override
	public Scene getScene() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(8);
		grid.setHgap(10);

		VBox mainBox = new VBox();
		mainBox.setAlignment(Pos.CENTER);
		mainBox.getChildren().add(super.getCombobox());

		Label brandLabel = new Label("Brand:");
		GridPane.setConstraints(brandLabel, 0, 1);

		TextField brandBox = new TextField();
		brandBox.setPromptText("Ex: Levit");
		GridPane.setConstraints(brandBox, 1, 1);

		Label priceLabel = new Label("Price:");
		GridPane.setConstraints(priceLabel, 0, 2);

		TextField priceBox = new TextField();
		priceBox.setPromptText("Ex: 300");
		GridPane.setConstraints(priceBox, 1, 2);

		Label materialLabel = new Label("Material");
		GridPane.setConstraints(materialLabel, 0, 3);

		ObservableList<String> materialOptions = FXCollections.observableArrayList(comboboxOptoionsMaterials);
		ComboBox<String> fluteMaterialsCombobox = new ComboBox<String>(materialOptions);
		GridPane.setConstraints(fluteMaterialsCombobox, 1, 3);
		fluteMaterialsCombobox.promptTextProperty().set("Material");

		Label guitarTypeLabel = new Label("Flute type:");
		GridPane.setConstraints(guitarTypeLabel, 0, 4);

		ObservableList<String> options = FXCollections.observableArrayList(comboboxOptoions);
		ComboBox<String> fluteTypeCombobox = new ComboBox<String>(options);
		GridPane.setConstraints(fluteTypeCombobox, 1, 4);
		fluteTypeCombobox.promptTextProperty().set("Type");

		Button addButton = new Button("Add");
		addButton.setOnAction((event) -> {
			try {
				super.verifyFieldData(brandBox, "Brand");
				super.verifyFieldData(priceBox, "Price");
				AfekaInstruments.getInstance()
						.addInstrument(new Flute(brandBox.getText(), Double.parseDouble(priceBox.getText()),
								fluteMaterialsCombobox.getValue().toString(), fluteTypeCombobox.getValue().toString()));
				super.close();
			} catch (Exception ex) {
				super.showError(ex);
			}
		});
		GridPane.setColumnSpan(addButton, 2);
		GridPane.setConstraints(addButton, 1, 5);
		GridPane.setConstraints(mainBox, 0, 0);

		grid.getChildren().addAll(brandLabel, brandBox, priceLabel, priceBox, materialLabel, fluteMaterialsCombobox,
				guitarTypeLabel, fluteTypeCombobox, addButton);

		mainBox.getChildren().add(grid);

		BorderPane.setAlignment(grid, Pos.BOTTOM_CENTER);

		return new Scene(mainBox, getWindow().getHeight(), getWindow().getHeight());

	}
}
