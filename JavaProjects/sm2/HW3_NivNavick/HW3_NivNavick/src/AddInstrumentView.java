//Niv Navick
//312164411
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class AddInstrumentView {
	
	private static final String ERROR_DIALOG_TITLE = "Error";
	private static final String EMPTY_FIELD_MESSAGE = "clould not be empty";
	private Stage window;
	private Stage newWindow;
	private Scene scene;
	private AddInstrumentComobox combobox;
	
	public AddInstrumentView(Stage primaryStage, Stage newWindow, String comboboxTitle) {
		window=primaryStage;
		this.newWindow=newWindow;
		InitizlizeNewWindow(comboboxTitle);
	}
	
	public Scene getScene() {
		return scene;
	}
	
	public Stage getWindow() {
		return window;
	}
	
	public void close() {
		newWindow.close();
	}
	
	public ComboBox getCombobox() {
		return combobox.getField();
	}
	
	public void showError(Exception ex) {
		showErrorDialog(ERROR_DIALOG_TITLE,ex.getMessage());
	}
	
	public void verifyFieldData(TextField box,String field) throws Exception {
		if(box.getText().isEmpty()) {
			throw new Exception(field+" "+EMPTY_FIELD_MESSAGE);
		}
	}
	
	public boolean validateCustomStringField(String data,String title,String message) {
		if(data.equals("") || data ==null) {
			showErrorDialog(title,message);
			return false;
		}
		return true;
	}
	
	private void showErrorDialog(String title,String message) {
		new PopupView(title,message,
				AlertType.ERROR,new Runnable()
		{
		    @Override
		    public void run()
		    {
		    	
		    }
		});
	}
	
	private void InitizlizeNewWindow(String comboboxTitle) {
		combobox=new AddInstrumentComobox(window,newWindow);
		combobox.setAction(null);
    	BorderPane borderPane = new BorderPane();
		getCombobox().setValue(comboboxTitle);
		
        scene = new Scene(borderPane, window.getHeight(), window.getHeight());
	}
}
