//Niv Navick
//312164411
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;

public class SearchBar {
	private static final String SEARCH_BAR_TEXT = "Search...";
	private static final String GO_BUTTON_TEXT = "Go";
	TextField searchField=new TextField();
	Button goButton =new Button(GO_BUTTON_TEXT);
	public SearchBar () {
		searchField.setPromptText(SEARCH_BAR_TEXT);
	}
	
	public String getText() {
		return searchField.getText();
	}
	
	public TextField getField() {
		return searchField;
	}
	
	public Button getButton() {
		return goButton;
	}
	
	public void setActionsToBoth(Runnable callback) {
		setSearchButtonAction(callback);
		setSearchBoxAction(KeyCode.ENTER,callback);
	}
	
	public void setSearchButtonAction(Runnable callback) {
		getButton().setOnAction((event) -> {
			callback.run();
		});
	}
	
	public void setSearchBoxAction(KeyCode code, Runnable callback) {
		getField().setOnKeyPressed(e -> {
			 if (e.getCode() == code && !getField().getText().isEmpty()) {
				 callback.run();
			 }
		});
	}
}
