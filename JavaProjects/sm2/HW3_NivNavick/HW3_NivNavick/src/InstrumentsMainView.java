//Niv Navick
//312164411
import java.util.ArrayList;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.Node;

public class InstrumentsMainView extends Application {

	private static final String NO_ITEMS = "No Items";
	private static final String MAIN_TITLE = "Afeka Instrument Music Store";
	private int pageIndex = 0;

	public static void main(String[] args) {
		launch();
	}

	Stage window;
	BorderPane borderPane = new BorderPane();
	TextField type = new TextField();
	TextField brand = new TextField();
	TextField price = new TextField();

	public InstrumentsMainView() {
		if (AfekaInstruments.getInstance().getSize() > 0) {
			nextPage();
		}
	}

	private void nextPage() {
		if(!continueBasicPaging()) {
			return;
		}
		if (pageIndex + 1 >= AfekaInstruments.getInstance().getSize()) {
			pageIndex = 0;
		} else {
			pageIndex++;
		}
		updateView(AfekaInstruments.getInstance().getByIndex(pageIndex));
	}

	private void previousPage() {
		if(!continueBasicPaging()) {
			return;
		}
		
		if (pageIndex - 1 <= -1) {
			pageIndex = AfekaInstruments.getInstance().getSize() - 1;
		} else {
			pageIndex--;
		}
		updateView(AfekaInstruments.getInstance().getByIndex(pageIndex));
	}
	

	private boolean continueBasicPaging() {
		if (AfekaInstruments.getInstance().getSize() == 0) {
			return false;
		}
		else if(AfekaInstruments.getInstance().getSize() == 1) {
			pageIndex=0;
		}
		return true;
	}

	private void updateView(MusicalInstrument ins) {
		type.setText(ins.getClass().getSimpleName());
		brand.setText(ins.getBrand());
		price.setText(ins.getPrice() + "");
	}

	private void setPromoText(String text) {
		type.setPromptText(text);
		price.setPromptText(text);
		brand.setPromptText(text);
	}

	private void clearTextBoxes() {
		type.clear();
		price.clear();
		brand.clear();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		new FilePathDialogView();

		Scene scene = new Scene(borderPane, 1000, 300);
		SearchBar sBar = new SearchBar();
		ActionButtons actionsButtons = new ActionButtons();
		dissableEditing();
		setKeyBindings(scene);

		window = primaryStage;
		window.setTitle(MAIN_TITLE);
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(12);
		grid.setHgap(12);

		HBox hBox = new HBox();
		HBox hBoxTopBar = new HBox();
		VBox vBox = new VBox();
		grid.setPadding(new Insets(10, 10, 10, 10));
		sBar.getField().setPrefWidth(900);

		sBar.setActionsToBoth(new Runnable() {
			@Override
			public void run() {
				boolean hasItems = AfekaInstruments.getInstance().search(sBar.getText());
				if (hasItems) {
					updateView(AfekaInstruments.getInstance().getByIndex(0));
					pageIndex = -1;
				}
			}
		});

		hBoxTopBar.setSpacing(7);
		hBoxTopBar.getChildren().addAll(sBar.getField(), sBar.getButton());
		hBoxTopBar.setAlignment(Pos.CENTER);
		hBoxTopBar.setPadding(new Insets(10, 10, 10, 10));

		createDynamicButtons(actionsButtons);

		ArrayList<Node> nodes = actionsButtons.getAll();
		VBox bottomvbox = new VBox();
		hBox.getChildren().addAll(actionsButtons.getAll().toArray(new Node[nodes.size()]));
		hBox.setAlignment(Pos.CENTER);
		hBox.setSpacing(10);
		hBox.setPadding(new Insets(10, 10, 20, 10));
		bottomvbox.getChildren().add(hBox);

		Label typeLabel = new Label("Type:");
		GridPane.setConstraints(typeLabel, 0, 0);

		Label brandLabel = new Label("Brand:");
		GridPane.setConstraints(brandLabel, 0, 1);

		Label priceLabel = new Label("Price:");
		GridPane.setConstraints(priceLabel, 0, 2);

		GridPane.setConstraints(type, 1, 0);
		GridPane.setConstraints(brand, 1, 1);
		GridPane.setConstraints(price, 1, 2);
		grid.setAlignment(Pos.CENTER);

		Button nextButton = new Button(">");
		nextButton.setOnAction((event) -> {
			nextPage();
		});

		GridPane.setConstraints(nextButton, 1, 3);

		Button prevButton = new Button("<");
		prevButton.setOnAction((event) -> {
			previousPage();
		});

		VBox vBoxNextButton = new VBox();

		vBoxNextButton.getChildren().add(prevButton);
		vBoxNextButton.setAlignment(Pos.CENTER);
		vBoxNextButton.setPadding(new Insets(13, 10, window.getHeight() + 20, 10));
		GridPane.setConstraints(vBoxNextButton, 0, 3);

		VBox vBoxPrevButton = new VBox();
		vBoxPrevButton.getChildren().add(nextButton);
		vBoxPrevButton.setAlignment(Pos.CENTER);
		vBoxPrevButton.setPadding(new Insets(10, 10, window.getHeight() + 20, 10));
		GridPane.setConstraints(vBoxPrevButton, 0, 3);

		TextAnimation textAnimation = new TextAnimation();
		HBox textAnimationBox = new HBox();
		textAnimationBox.getChildren().add(textAnimation.getTextAsAnimation());
		textAnimationBox.setAlignment(Pos.CENTER_LEFT);
		textAnimationBox.setPadding(new Insets(10, 10, 20, 10));

		bottomvbox.getChildren().add(textAnimationBox);

		grid.getChildren().addAll(type, brand, price, typeLabel, brandLabel, priceLabel);
		borderPane.setCenter(grid);
		borderPane.setLeft(vBoxNextButton);
		borderPane.setRight(vBoxPrevButton);
		borderPane.setTop(hBoxTopBar);
		borderPane.setBottom(bottomvbox);
		BorderPane.setAlignment(vBoxPrevButton, Pos.CENTER_LEFT);
		BorderPane.setAlignment(hBoxTopBar, Pos.TOP_CENTER);
		BorderPane.setAlignment(vBoxNextButton, Pos.CENTER_LEFT);
		BorderPane.setAlignment(vBox, Pos.BOTTOM_CENTER);
		borderPane.setCenter(grid);

		window.setScene(scene);
		window.show();
		updateView(AfekaInstruments.getInstance().getByIndex(0));
	}

	private void dissableEditing() {
		price.setEditable(false);
		type.setEditable(false);
		brand.setEditable(false);
	}

	private void createDynamicButtons(ActionButtons actionsButtons) {
		actionsButtons.createButton("Clear", "Clear", new Runnable() {
			@Override
			public void run() {
				AfekaInstruments.getInstance().removeAll();
				clearTextBoxes();
				setPromoText(NO_ITEMS);
			}
		});

		actionsButtons.createButton("Delete", "Delete", new Runnable() {
			@Override
			public void run() {
				deleteInstrument();
			}
		});

		actionsButtons.createButton("Add", "Add", new Runnable() {
			@Override
			public void run() {
				new AddInstrumentiePreView(window);
			}
		});
	}

	private void setKeyBindings(Scene scene) {
		scene.setOnKeyReleased(event -> {
			switch (event.getCode()) {
			case RIGHT:
				nextPage();
				break;
			case LEFT:
				previousPage();
				break;
			case DELETE:
				deleteInstrument();
				break;
			case A:
				new AddInstrumentiePreView(window);
				break;
			default:
				break;
			}
		});
	}

	private void deleteInstrument() {
		AfekaInstruments.getInstance().removeByIndex(pageIndex);
		if (pageIndex > 0) {
			pageIndex--;
		}
		if (AfekaInstruments.getInstance().getSize() == 0) {
			clearTextBoxes();
			setPromoText(NO_ITEMS);
		} else {
			updateView(AfekaInstruments.getInstance().getByIndex(pageIndex));
		}
	}
}
