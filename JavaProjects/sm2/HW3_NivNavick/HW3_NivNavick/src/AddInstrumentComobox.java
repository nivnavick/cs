//Niv Navick
//312164411
import java.util.Arrays;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

public class AddInstrumentComobox {

	private static final String INSTRUMENT_COMBOBOX_PROM_TEXT = "Choose Instrument Type Here";
	final private String[] comboboxOptoions = new String[] { "Guitar", "Bass", "Flute", "Saxophone" };
	private static AddInstrumentView[] views;
	private AddInstrumentView currentView;
	private ComboBox<String> comboBox;
	private Stage newWindow1;

	public AddInstrumentComobox(Stage window, Stage newWindow) {
		newWindow1=newWindow;
		if (views == null) {
			views = new AddInstrumentView[0];
			views = new AddInstrumentView[] { new AddGuitarView(window,newWindow), new AddBassView(window,newWindow),
					new AddFluteView(window,newWindow), new AddSaxophoneView(window,newWindow) };
		}
		initializeCombobox();
	}

	public void setAction(Runnable callback) {
		comboBox.setOnAction((event) -> {
			int index=Arrays.asList(comboboxOptoions).indexOf(getField().getValue());
			newWindow1.setScene(views[index].getScene());
		});
	}

	public AddInstrumentView getCurrentView() {
		return currentView;
	}

	private void initializeCombobox() {
		ObservableList<String> options = FXCollections.observableArrayList(comboboxOptoions);
		comboBox = new ComboBox<String>(options);
		comboBox.promptTextProperty().set(INSTRUMENT_COMBOBOX_PROM_TEXT);
		comboBox.setPrefWidth(250);
		setAction(null);
	}

	public ComboBox<String> getField() {
		return comboBox;
	}
}
