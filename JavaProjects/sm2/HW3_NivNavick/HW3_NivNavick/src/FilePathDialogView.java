//Niv Navick
//312164411
import java.io.File;
import java.util.Optional;

import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;

public class FilePathDialogView {
		public FilePathDialogView() {
			TextInputDialog dialog = new TextInputDialog("/Users/nivnavick/Documents/Git/CS/JavaProjects/sm2/HW3_NivNavick/HW3_NivNavick/src/instruments1b.txt");
			dialog.setTitle("File Path");
			dialog.setHeaderText("Load Instruments From File");
			dialog.setContentText("Please enter file name");

			Optional<String> result = dialog.showAndWait();
			if (result.isPresent()) {
				checkFileExistance(result.get());
				dialog.close();
			} else {
			    System.exit(0);
			}
		}
		
		private void checkFileExistance(String path) {
			File file = new File(path);
            if(!file.exists() || !file.canRead()) {		
        		new PopupView("Error","Cannot read from file,please try again",
        				AlertType.ERROR,new Runnable()
        		{
        		    @Override
        		    public void run()
        		    {
        		    	new FilePathDialogView();
        		    }
        		});
            }
            else {
            	AfekaInstruments.getInstance().loadInstrumentsFromFile(file);
            	new InstrumentsMainView();
            }
		}
}
