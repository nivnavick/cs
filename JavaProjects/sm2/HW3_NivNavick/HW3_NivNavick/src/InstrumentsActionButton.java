//Niv Navick
//312164411
import javafx.scene.control.Button;

public class InstrumentsActionButton extends Button {

	public InstrumentsActionButton(String message,Runnable callback) {
		setText(message);
		setAction(callback);
	}
	
	public void setAction(Runnable callback) {
		setOnAction((event) -> {
			runCallback(callback);
		});
	}
	
	private void runCallback(Runnable callback)
	{
	    callback.run();
	}
	
}
