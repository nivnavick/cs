import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AddGuitarView extends AddInstrumentView {

	private static final String COMBOBOX_TITLE = "Guitar";
	final private String[] comboboxOptoions = new String[] {"Classic", "Acoustic",  "Electric"};

	public AddGuitarView(Stage primaryStage, Stage newWindow) {
		super(primaryStage, newWindow,COMBOBOX_TITLE);
	}

	@Override
	public Scene getScene() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(8);
		grid.setHgap(10);
		
		VBox mainBox=new VBox();
		mainBox.setAlignment(Pos.CENTER);
		mainBox.getChildren().add(super.getCombobox());
		
		Label brandLabel = new Label("Brand:");
		GridPane.setConstraints(brandLabel, 0, 1);

		TextField brandBox = new TextField();
		brandBox.setPromptText("Ex: Gibson");
		GridPane.setConstraints(brandBox, 1, 1);

		Label priceLabel = new Label("Price:");
		GridPane.setConstraints(priceLabel, 0, 2);

		TextField priceBox = new TextField();
		priceBox.setPromptText("Ex: 7500");
		GridPane.setConstraints(priceBox, 1, 2);

		Label numOfStringLabel = new Label("Number of string:");
		GridPane.setConstraints(numOfStringLabel, 0, 3);

		TextField stringsBox = new TextField();
		stringsBox.setPromptText("Ex: 6");
		GridPane.setConstraints(stringsBox, 1, 3);

		Label guitarTypeLabel = new Label("Guitar type:");
		GridPane.setConstraints(guitarTypeLabel, 0, 4);

		ObservableList<String> options = FXCollections.observableArrayList(comboboxOptoions);
		ComboBox<String> guitarTypeCombobox = new ComboBox<String>(options);
		GridPane.setConstraints(guitarTypeCombobox, 1, 4);
		guitarTypeCombobox.promptTextProperty().set("Type");

		Button addButton = new Button("Add");
		addButton.setOnAction((event) -> {
			
			try {
				super.verifyFieldData(brandBox, "Brand");
				super.verifyFieldData(priceBox, "Price");
				super.verifyFieldData(stringsBox, "Price");
				AfekaInstruments.getInstance()
						.addInstrument(new Guitar(brandBox.getText(), Double.parseDouble(priceBox.getText()),
								Integer.parseInt(stringsBox.getText()), guitarTypeCombobox.getValue().toString()));
				super.close();
			}
			catch(Exception ex) {
				super.showError(ex);
			}
		});
		
		GridPane.setConstraints(mainBox, 0, 0);
		GridPane.setColumnSpan(addButton, 2);
		GridPane.setConstraints(addButton, 1, 5);

		grid.getChildren().addAll(brandLabel, brandBox, priceLabel, numOfStringLabel, stringsBox,
				guitarTypeLabel, guitarTypeCombobox, priceBox, addButton);

		mainBox.getChildren().add(grid);
		return new Scene(mainBox, getWindow().getHeight(), getWindow().getHeight());
	}
}
