import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class AfekaInstruments {

	private static AfekaInstruments Instance;
	private int size=0;
	private ArrayList<MusicalInstrument> allInstruments;
	ArrayList<MusicalInstrument> filteredInstruments;
	boolean searchMode=false;
	
    private AfekaInstruments() {
    	allInstruments = new ArrayList<MusicalInstrument>();
    	filteredInstruments = new ArrayList<MusicalInstrument>();
    }
    
    public static AfekaInstruments getInstance() {
    	if(Instance==null) {
    	//	loaded=true;
    		Instance=new AfekaInstruments();
    	}
    	return Instance;
    }
    
    public void addInstrument(MusicalInstrument instrument) {
    	allInstruments.add(instrument);
    	size++;
    }
    
    public boolean search(String text) {
    	if(size==0) {
    		return false;
    	}
    	filteredInstruments.removeAll(filteredInstruments);
    	if(text.equals("")) {
    		searchMode=false;
    	}
    	else {
    		searchMode=true;
    	}
    	
    	for(MusicalInstrument d:allInstruments) {
    		if(d.toString().contains(text)) {
        		filteredInstruments.add(d);
    		}
    	}
    	
    	if(filteredInstruments.size()==0) {
    		searchMode=false;
    		return true;
    	}
    	return true;
    }
    
    public static File getInstrumentsFileFromUser(){
        boolean stopLoop = true;
        File file;
        Scanner consoleScanner = new Scanner(System.in);

        do {
            System.out.println("Please enter instruments file name / path:");
            String filepath = consoleScanner.nextLine();
            file = new File(filepath);
            stopLoop = file.exists() && file.canRead();

            if(!stopLoop)
                System.out.println("\nFile Error! Please try again\n\n");
        }while (!stopLoop);

        return file;
    }
    
    public int getSize() {
    	if(searchMode) {
    		return filteredInstruments.size();
    	}
    	return allInstruments.size();
    }

    public void loadInstrumentsFromFile(File file){
        Scanner scanner = null;

        try {

            scanner = new Scanner(file);

            addAllInstruments(allInstruments ,loadGuitars(scanner));

            addAllInstruments(allInstruments ,loadBassGuitars(scanner));

            addAllInstruments(allInstruments ,loadFlutes(scanner));

            addAllInstruments(allInstruments ,loadSaxophones(scanner));

        }catch (InputMismatchException | IllegalArgumentException ex){
            System.err.println("\n"+ ex.getMessage());
            System.exit(1);
        }catch (FileNotFoundException ex){
            System.err.println("\nFile Error! File was not found");
            System.exit(2);
        } finally {
            scanner.close();
        }
        System.out.println("\nInstruments loaded from file successfully!\n");
        size=allInstruments.size();

    }
    
    public void removeByIndex(int index) {
    	if(size>0) {
    		allInstruments.remove(index);
    		size--;
    	}
    }
    
	public void removeAll() {
		allInstruments.removeAll(allInstruments);
		size=0;
	}
    
    public MusicalInstrument getByIndex(int index) {
    	if(getSize()==0) {
    		return null;
    	}
     	
    	if(searchMode) {
        	return (MusicalInstrument) filteredInstruments.get(index);
    	}
    	return (MusicalInstrument) allInstruments.get(index);
    }

    private ArrayList loadGuitars(Scanner scanner){
        int numOfInstruments = scanner.nextInt();
        ArrayList guitars = new ArrayList(numOfInstruments);

        for(int i = 0; i < numOfInstruments ; i++)
            guitars.add(new Guitar(scanner));

        return guitars;
    }

    private ArrayList loadBassGuitars(Scanner scanner){
        int numOfInstruments = scanner.nextInt();
        ArrayList bassGuitars = new ArrayList(numOfInstruments);

        for(int i = 0; i < numOfInstruments ; i++)
            bassGuitars.add(new Bass(scanner));

        return bassGuitars;
    }

    private ArrayList loadFlutes(Scanner scanner){
        int numOfInstruments = scanner.nextInt();
        ArrayList flutes = new ArrayList(numOfInstruments);

        for(int i = 0; i < numOfInstruments ; i++)
            flutes.add(new Flute(scanner));


        return flutes;
    }

    private ArrayList loadSaxophones(Scanner scanner){
        int numOfInstruments = scanner.nextInt();
        ArrayList saxophones = new ArrayList(numOfInstruments);

        for(int i = 0; i < numOfInstruments ; i++)
            saxophones.add(new Saxophone(scanner));

        return saxophones;
    }

    public static void addAllInstruments(ArrayList instruments, ArrayList moreInstruments){
        for(int i = 0 ; i < moreInstruments.size() ; i++){
            instruments.add(moreInstruments.get(i));
        }
    }

    public static void printInstruments(ArrayList instruments){
        for(int i = 0 ; i < instruments.size() ; i++)
            System.out.println(instruments.get(i));
    }



    public static int getNumOfDifferentElements(ArrayList instruments){
        int numOfDifferentInstruments;
        ArrayList differentInstruments = new ArrayList();
        System.out.println();

        for(int i = 0 ; i < instruments.size() ; i++){
            if(!differentInstruments.contains((instruments.get(i)))){
                differentInstruments.add(instruments.get(i));
            }
        }

        if(differentInstruments.size() == 1)
            numOfDifferentInstruments = 0;

        else
            numOfDifferentInstruments = differentInstruments.size();


        return numOfDifferentInstruments;
    }

    public static MusicalInstrument getMostExpensiveInstrument(ArrayList instruments){
        double maxPrice = 0;
        MusicalInstrument mostExpensive = (MusicalInstrument) instruments.get(0);

        for(int i = 0 ; i < instruments.size() ; i++){
            MusicalInstrument temp = (MusicalInstrument)instruments.get(i);

            if(temp.getPrice() > maxPrice){
                maxPrice = temp.getPrice();
                mostExpensive = temp;
            }
        }

        return mostExpensive;
    }
}
