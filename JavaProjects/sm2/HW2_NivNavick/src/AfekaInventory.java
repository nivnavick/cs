import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AfekaInventory <T extends MusicalInstrument>implements IAfekaInventory<T> {

    private ArrayList<T> array=new ArrayList<T>();
    private Boolean isSorted=false;
    private double totalPrice=0;

    @Override
    public <E extends MusicalInstrument> void addAllStringInstruments(ArrayList<? extends T> fromArray, ArrayList<? super E> toArray) {
        for(MusicalInstrument musicalInstrument:fromArray) {
            if(musicalInstrument instanceof StringInstrument) {
                addInstrument((E)musicalInstrument,toArray);
            }
        }
    }

    @Override
    public  <E extends MusicalInstrument> void addAllWindInstruments(ArrayList<? extends T> fromArray, ArrayList<? super E> toArray) {
        for(MusicalInstrument musicalInstrument:fromArray) {
            if(musicalInstrument instanceof WindInstrument) {
                addInstrument((E)musicalInstrument,toArray);
            }
        }
    }

    private void sumTotalPrice(double price) {
        this.totalPrice+=price;
    }

    @Override
    public void sortByBrandAndPrice(ArrayList<MusicalInstrument> arrayToSort) {
        for (int i = arrayToSort.size()- 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arrayToSort.get(j).compareTo(arrayToSort.get(j+1))>0)
                    swap(arrayToSort, j, j + 1);
            }
        }
        setSorted(true);
    }

    private static void swap(ArrayList<MusicalInstrument> arr, int i, int j) {
        //Swap cells (helper method of bubble sort)
        MusicalInstrument temp = arr.get(i);
        arr.set(i,arr.get(j));
        arr.set(j,temp);
    }

    @Override
    public int binarySearchByBrandAndPrice(ArrayList<MusicalInstrument> arrayToSearch,String brand,Number price) {
        MusicalInstrument m= new MusicalInstrument(brand,price) { };
        int low = 0, high = arrayToSearch.size() - 1, middle;
        while (low <= high) {
            middle = (low + high) / 2;
            if (arrayToSearch.get(middle).compareTo(m)>0)
                high = middle - 1;
            else if (arrayToSearch.get(middle).compareTo(m)<0)
                low = middle + 1;
            else if (arrayToSearch.get(middle).compareTo(m)==0)
               return middle;
        }
        return -1;
    }

    @Override
    public <E extends MusicalInstrument>  void addInstrument(MusicalInstrument musicalInstrument,ArrayList<? super E> array) {
        array.add((E)musicalInstrument);
        sumTotalPrice(musicalInstrument.getPrice().doubleValue());
    }

    @Override
    public boolean removeInstrument(MusicalInstrument musicalInstrument,ArrayList<MusicalInstrument> array) {
        array.remove(binarySearchByBrandAndPrice(array,musicalInstrument.getBrand(),musicalInstrument.getPrice()));
        sumTotalPrice(-musicalInstrument.getPrice().doubleValue());//Reduce total price
        return true;
    }

    @Override
    public boolean removeAll(ArrayList<MusicalInstrument> array) {
        array.clear();
        return true;
    }

    public double getPrice(Number price1,Number price2) {
        return price1.doubleValue()+price2.doubleValue();
    }

    public double getTotalPrice() {
        return this.totalPrice;
    }

    public ArrayList<T> getInnerArray() {
        return this.array;
    }

    public boolean getSorted() {
        return this.isSorted;
    }

    public void setSorted(boolean value) {
        this.isSorted=true;
    }
}
