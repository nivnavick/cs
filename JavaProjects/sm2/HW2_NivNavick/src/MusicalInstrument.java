import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Scanner;

public abstract class MusicalInstrument implements InstrumentFunc{
    private Number price;
    private String brand;

    public MusicalInstrument(String brand, Number price){
        setBrand(brand);
        setPrice(price);
    }

    public MusicalInstrument(Scanner scanner){
        Number price;
        String brand;

        try {
           price=scanner.nextDouble();
        }catch (InputMismatchException ex){
            throw new InputMismatchException("Price not found!");
        }
        setPrice(price);
        scanner.nextLine();
        brand = scanner.nextLine();
        setBrand(brand);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Number getPrice() {
        return price;
    }

    public void setPrice(Number price) {
        if(price.floatValue() > 0)
            this.price = price;
        else
            throw new InputMismatchException("Price must be a positive number!");

    }


    protected boolean isValidType(String[] typeArr, String material){
        for(int i = 0; i < typeArr.length ; i++) {
            if (material.equals(typeArr[i])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof MusicalInstrument))
            return false;

        MusicalInstrument otherInstrument = (MusicalInstrument) o;

        return getPrice() == otherInstrument.getPrice() && getBrand().equals(otherInstrument.getBrand());
    }


    @Override
    public String toString() {
        if(price.doubleValue()-price.intValue()==0.0) {
            return String.format("%-8s %-9s| Price: %9d,", getBrand(), getClass().getCanonicalName(), getPrice().intValue());
        }
        return String.format("%-8s %-9s| Price: %9.2f,", getBrand(), getClass().getCanonicalName(), getPrice().doubleValue());
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int compareTo(Object o) {
        String givenObject=((MusicalInstrument)o).getBrand();
        String current=getBrand();
        if(current.compareToIgnoreCase(givenObject)==0) {
            if(getPrice().doubleValue()>((MusicalInstrument)o).getPrice().doubleValue()){
                return 1;
            }
            else if(getPrice().doubleValue()<((MusicalInstrument)o).getPrice().doubleValue()) {
                return -1;
            }
            return 0;
        }
        else if(current.compareToIgnoreCase(givenObject)>0) {
            return 1;
        }
        return -1;
    }
}
