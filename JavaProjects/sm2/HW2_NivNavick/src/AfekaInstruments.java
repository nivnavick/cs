import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class AfekaInstruments {

    public static void main(String[] args) {

        ArrayList<MusicalInstrument> allInstruments = new ArrayList<>();
        File file = getInstrumentsFileFromUser();

        loadInstrumentsFromFile(file, allInstruments);

        if (allInstruments.size() == 0) {
            System.out.println("There are no instruments in the store currently");
            return;
        }

        printInstruments(allInstruments);

        int different = getNumOfDifferentElements(allInstruments);

        System.out.println("\n\nDifferent Instruments: " + different);

        MusicalInstrument mostExpensive = getMostExpensiveInstrument(allInstruments);

        System.out.println("\n\nMost Expensive Instrument:\n" + mostExpensive);
        printMenuOptions();
        startInventoryMenu(allInstruments);
    }

    private static void printMenuOptions() {
        String[] options = {
                "Copy All String Instruments To Inventory",
                "Copy All Wind Instruments To Inventory",
                "Sort Instruments By Brand And Price",
                "Search Instrument By Brand And Price",
                "Delete Instrument",
                "Delete all Instruments",
                "Print Inventory Instruments"
        };
        System.out.println("-------------------------------------------------------------------------");
        System.out.println("AFEKA MUSICAL INSTRUMENT INVENTORY MENU");
        System.out.println("-------------------------------------------------------------------------");
        for (int i = 0; i < options.length; i++) {
            System.out.println(String.format("%d. %s",i+1, options[i]));
        }
        System.out.println("Choose your option or any other key to EXIT");
    }

    private static <T extends MusicalInstrument> void printAfekaInventory(AfekaInventory<T> afekaInventory) {
        System.out.println("-------------------------------------------------------------------------");
        System.out.println("AFEKA MUSICAL INSTRUMENTS INVENTORY");
        System.out.println("-------------------------------------------------------------------------");
        if(afekaInventory.getInnerArray().size()==0) {
            System.out.println("There Is No Instruments To Show");
        }
        printInstruments(afekaInventory.getInnerArray());
        System.out.println(String.format("\nTotal Price:   %.2f      Sorted: %b",afekaInventory.getTotalPrice(),afekaInventory.getSorted()));
    }

    private static void startInventoryMenu(ArrayList<MusicalInstrument> allInstruments) {
        AfekaInventory<MusicalInstrument> afekaInventory =new AfekaInventory<>();
        Scanner scanner = new Scanner(System.in);
        int option;
        boolean breakOnUserKey=false;
        do {
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    afekaInventory.addAllStringInstruments(allInstruments, afekaInventory.getInnerArray());
                    System.out.println("All String Instruments Added Successfully!");
                    break;
                case 2:
                    afekaInventory.addAllWindInstruments(allInstruments, afekaInventory.getInnerArray());
                    System.out.println("All Wind Instruments Added Successfully!");
                    break;
                case 3:
                    afekaInventory.sortByBrandAndPrice(afekaInventory.getInnerArray());
                    System.out.print("Instruments Sorted Successfully!");
                    break;
                case 4:
                    System.out.print("Brand:");
                    String brand = scanner.next();
                    System.out.print("Price:");
                    double price = scanner.nextDouble();
                    int x=afekaInventory.binarySearchByBrandAndPrice(afekaInventory.getInnerArray(), brand, price);
                    if(x==-1) {
                        System.out.print("Instrument Not Found!");
                    }
                    else {
                        System.out.print("Result:");
                        System.out.println(afekaInventory.getInnerArray().get(x));
                    }
                    break;
                case 5:
                    System.out.println("DELETE INSTRUMENT:");
                    System.out.print("Brand:");
                    String brand1 = scanner.next();
                    System.out.print("Price:");
                    double price1 = scanner.nextDouble();
                    int x1=afekaInventory.binarySearchByBrandAndPrice(afekaInventory.getInnerArray(), brand1, price1);
                    if(x1==-1) {
                        System.out.print("Instrument Not Found!");
                    }
                    else {
                        System.out.print("Result:");
                        System.out.println(afekaInventory.getInnerArray().get(x1));
                        String s = scanner.nextLine();
                        if (s != null) {
                            s = scanner.nextLine();
                        }
                        if (s.toLowerCase().charAt(0) == 'y') {
                            afekaInventory.removeInstrument(afekaInventory.getInnerArray().get(x1), afekaInventory.getInnerArray());
                            System.out.println("Instrument Deleted Successfully!");
                        }
                    }
                    break;
                case 6:
                    System.out.println("DELETE ALL INSTRUMENTS:");
                    System.out.print("Are You Sure?(Y/N)");
                    String s1=scanner.nextLine();
                    if(s1!=null) {
                        s1=scanner.nextLine();
                    }
                    if(s1.toLowerCase().charAt(0)=='y') {
                        afekaInventory.removeAll(afekaInventory.getInnerArray());
                        System.out.println("All Instruments Deleted Successfully!");
                    }
                    break;
                case 7:
                    printAfekaInventory(afekaInventory);
                    break;
                default:
                    breakOnUserKey=true;
                    System.out.println("Finished!");
                    break;
            }
        }
        while (!breakOnUserKey);
    }

    public static File getInstrumentsFileFromUser() {
        boolean stopLoop = true;
        File file;
        Scanner consoleScanner = new Scanner(System.in);

        do {
            System.out.println("Please enter instruments file name / path:");
            String filepath = consoleScanner.nextLine();
            file = new File(filepath);
            stopLoop = file.exists() && file.canRead();

            if (!stopLoop)
                System.out.println("\nFile Error! Please try again\n\n");
        } while (!stopLoop);

        return file;
    }

    public static void loadInstrumentsFromFile(File file, ArrayList allInstruments) {
        Scanner scanner = null;

        try {

            scanner = new Scanner(file);

            addAllInstruments(allInstruments, loadGuitars(scanner));

            addAllInstruments(allInstruments, loadBassGuitars(scanner));

            addAllInstruments(allInstruments, loadFlutes(scanner));

            addAllInstruments(allInstruments, loadSaxophones(scanner));

        } catch (InputMismatchException | IllegalArgumentException ex) {
            System.err.println("\n" + ex.getMessage());
            System.exit(1);
        } catch (FileNotFoundException ex) {
            System.err.println("\nFile Error! File was not found");
            System.exit(2);
        } finally {
            scanner.close();
        }
        System.out.println("\nInstruments loaded from file successfully!\n");

    }

    public static ArrayList loadGuitars(Scanner scanner) {
        int numOfInstruments = scanner.nextInt();
        ArrayList guitars = new ArrayList(numOfInstruments);

        for (int i = 0; i < numOfInstruments; i++)
            guitars.add(new Guitar(scanner));

        return guitars;
    }

    public static ArrayList loadBassGuitars(Scanner scanner) {
        int numOfInstruments = scanner.nextInt();
        ArrayList bassGuitars = new ArrayList(numOfInstruments);

        for (int i = 0; i < numOfInstruments; i++)
            bassGuitars.add(new Bass(scanner));

        return bassGuitars;
    }

    public static ArrayList loadFlutes(Scanner scanner) {
        int numOfInstruments = scanner.nextInt();
        ArrayList flutes = new ArrayList(numOfInstruments);

        for (int i = 0; i < numOfInstruments; i++)
            flutes.add(new Flute(scanner));


        return flutes;
    }

    public static ArrayList loadSaxophones(Scanner scanner) {
        int numOfInstruments = scanner.nextInt();
        ArrayList saxophones = new ArrayList(numOfInstruments);

        for (int i = 0; i < numOfInstruments; i++)
            saxophones.add(new Saxophone(scanner));

        return saxophones;
    }

    public static void addAllInstruments(ArrayList instruments, ArrayList moreInstruments) {
        for (int i = 0; i < moreInstruments.size(); i++) {
            instruments.add(moreInstruments.get(i));
        }
    }

    public static void printInstruments(ArrayList instruments) {
        for (int i = 0; i < instruments.size(); i++)
            System.out.println(instruments.get(i));
    }


    public static int getNumOfDifferentElements(ArrayList instruments) {
        int numOfDifferentInstruments;
        ArrayList differentInstruments = new ArrayList();
        System.out.println();

        for (int i = 0; i < instruments.size(); i++) {
            if (!differentInstruments.contains((instruments.get(i)))) {
                differentInstruments.add(instruments.get(i));
            }
        }

        if (differentInstruments.size() == 1)
            numOfDifferentInstruments = 0;

        else
            numOfDifferentInstruments = differentInstruments.size();


        return numOfDifferentInstruments;
    }

    public static MusicalInstrument getMostExpensiveInstrument(ArrayList instruments) {
        double maxPrice = 0;
        MusicalInstrument mostExpensive = (MusicalInstrument) instruments.get(0);

        for (int i = 0; i < instruments.size(); i++) {
            MusicalInstrument temp = (MusicalInstrument) instruments.get(i);
            if (temp.getPrice().floatValue() > maxPrice) {
                maxPrice = temp.getPrice().floatValue();
                mostExpensive = temp;
            }
        }

        return mostExpensive;
    }

}
