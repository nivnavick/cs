import java.util.ArrayList;

public interface IAfekaInventory<T extends MusicalInstrument> {
    <E extends MusicalInstrument> void addAllStringInstruments(ArrayList<? extends T> fromArray, ArrayList<? super E> toArray);
    <E extends MusicalInstrument> void addAllWindInstruments(ArrayList<? extends T> fromArray, ArrayList<? super E> toArray);
    void sortByBrandAndPrice(ArrayList<MusicalInstrument> arrayToSort);
    int binarySearchByBrandAndPrice(ArrayList<MusicalInstrument> arrayToSearch,String brand,Number price);
    <E extends MusicalInstrument>  void addInstrument(MusicalInstrument musicalInstrument,ArrayList<? super E> array);
    boolean removeInstrument(MusicalInstrument musicalInstrument,ArrayList<MusicalInstrument> array);
    boolean removeAll(ArrayList<MusicalInstrument> array);
}
