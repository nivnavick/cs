import java.util.Scanner;

public class Bass extends StringInstrument {
    private final String INVALID_FRETLESS_ERR="Whether a bass is fretless or not is boolean, any other string than \"True\" or \"False\" is not acceptable";
    private boolean fretless;


    public Bass(double price, String manufacturer, int stringCount,boolean fretless) throws AfekaInstrumentsException {
        super(price, manufacturer, stringCount,"Bass");
        setFretless(fretless);
    }

    public Bass(Scanner scanner) throws AfekaInstrumentsException {
        super(scanner,"Bass");
        setFretless(scanner.nextLine());
    }

    public void setFretless(String fretless) throws AfekaInstrumentsException {

        if(!fretless.toLowerCase().equals("false") && !fretless.toLowerCase().equals("true")) {
            throw new AfekaInstrumentsException(INVALID_FRETLESS_ERR);
        }
        this.fretless=Boolean.parseBoolean(fretless);
    }

    public void setFretless(Boolean fretless) {
        this.fretless=fretless;
    }

    public boolean isFretless() {
        return this.fretless;
    }

    @Override
    public String toString() {

        return String.format("%-10s %-10s |%s| Fretless: %-19s",
                super.getManufacturer(),
                this.getClass().getSimpleName(),
                super.toString(),
                fretless?"Yes":"No");
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Bass && super.equals(obj) && ((Bass) obj).fretless == this.fretless;
    }
}
