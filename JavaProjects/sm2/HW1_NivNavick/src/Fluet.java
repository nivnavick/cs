import java.util.Scanner;

public class Fluet extends WindInstrument {
   private InstrumentType type;

    public Fluet(double price, String manufacturer,String material,String type) throws AfekaInstrumentsException {
        super(price, manufacturer,material);
        setType(type);
    }

    public Fluet(Scanner scanner) throws AfekaInstrumentsException {
     //   super(Double.parseDouble(scanner.nextLine()), scanner.nextLine(),scanner.nextLine());
        super(scanner);
        setType(scanner.nextLine());
    }

    public void setType(String type) {
        this.type=InstrumentType.valueOf(type);
    }

    public InstrumentType getType() {
        return this.type;
    }


    @Override
    public String toString() {
        return String.format("%-10s %-10s |%s| Type: %s",
                super.getManufacturer(),
                this.getClass().getSimpleName(),
                super.toString(),
                this.getType().name());
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Fluet && super.equals(obj);
    }
}
