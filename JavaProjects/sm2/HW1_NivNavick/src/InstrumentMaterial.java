public enum InstrumentMaterial {
    Metal,
    Wood,
    Plastic
}